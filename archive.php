<? get_header(); ?>
  <section class="wrapper archive mt-header">
    <div class="archive-results-inner">
      <ul>
      <?
        if (have_posts()) {

          while ( have_posts() ) {
            the_post(); 
      ?>

      <li class="archive-result-line-item">
        <a class="text-link" href="<? the_permalink(); ?>">
          <? the_title(); ?>
        </a>
      </li>
        <?
          } // end while 
        ?>
          </ul>
      <? 
        } else {
      ?>
        <div class="searchbox">
          <? get_search_form(); ?>
        </div>
        <p>Oops! Looks like there are no results for "<? echo get_search_query(); ?>"</p>
      <?
        } // end else
      ?>
    </div>

  </section>

<? get_footer(); ?>