<? get_header(); ?>
<div class="page-title-container">
  <svg viewBox="0 0 500 500">
    <path id="curve" d="M100,250 C100,72 395,74 400,250" />
      <text width="500">
        <textPath startOffset="50%" text-anchor="middle" xlink:href="#curve">
          <? the_title(); ?>
        </textPath>
      </text>
    </svg>
</div>
<? 
  global $wp;
  $current_slug = add_query_arg( array(), $wp->request );
  $home = "home";
?>
<? if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="wrapper page-content-<? if( is_home() || is_front_page() ) { 
      echo $home ;
    } else {
     echo $current_slug;
  } ?>">
  <? the_content(); ?>
</div>
<? endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<? endif; ?>
<? get_footer(); ?>