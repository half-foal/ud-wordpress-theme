
// can't do $(document). wordpress doesn't like it.
jQuery(document).ready(function ($) {

  let $menuIcon = '#header-global .js-menu-icon';
  let mobileMenu = {
    onMenuClick: function (event) {
      event.preventDefault();
      let isMenuOpen = $("html").hasClass("menu-is-opened");
      if (!isMenuOpen) {
        mobileMenu.openMenu();
      } else {
        mobileMenu.closeMenu();
      }
    },
    openMenu: function () {
      $("html").addClass("menu-is-opened");
    },
    closeMenu: function () {
      $("html").removeClass("menu-is-opened");
    },
    init: function () {
      $(document).on("click", $menuIcon, mobileMenu.onMenuClick);
    }
  }

  mobileMenu.init();





  let dinnerMenuSelector = '.menu-section-dinner',
    appsMenuSelector = '.menu-section-appetizers',
    sidesMenuSelector = '.menu-section-sides',
    sandwichesMenuSelector = '.menu-section-sandwiches';


  let $dinnerMenuContentSelector = $(dinnerMenuSelector).find('.js-menu-content'),
    $appsMenuContentSelector = $(appsMenuSelector).find('.js-menu-content'),
    $sidesMenuContentSelector = $(sidesMenuSelector).find('.js-menu-content'),
    $sandwichesMenuContentSelector = $(sandwichesMenuSelector).find('.js-menu-content');

  let $dinnerMenuParSelector = $dinnerMenuContentSelector.find('p'),
    $appsMenuParSelector = $appsMenuContentSelector.find('p'),
    $sidesMenuParSelector = $sidesMenuContentSelector.find('p'),
    $sandwichesMenuParSelector = $sandwichesMenuContentSelector.find('p');

  let $dinnerMenuImageSelector = $(dinnerMenuSelector).find('.image-content'),
    $appsMenuImageSelector = $(appsMenuSelector).find('.image-content'),
    $sidesMenuImageSelector = $(sidesMenuSelector).find('.image-content'),
    $sandwichesMenuImageSelector = $(sandwichesMenuSelector).find('.image-content');


  //front page menu 
  let restaurantFrontPageMenu = {
    replaceDinnerContents: function () {
      let arr = createArrFromText($dinnerMenuParSelector, ', ');
      console.log(arr);
      let menuLines = chunkArr(arr, 3);
      console.log(menuLines);
      let newHTML = $.map(menuLines, function (value) {
        let newValue = value.join("  ---  ");
        return ('<p class="menu-text"><span>' + newValue + '</span></p>');
      });
      $(dinnerMenuSelector).html(newHTML);
    },
    replaceAppsContents: function () {
      console.log("sides function started");
      let menuItems = createArrFromText($appsMenuParSelector, ', ');
      $appsMenuContentSelector.empty();
      $appsMenuImageSelector.empty();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($appsMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text">' + value + '</span>');
      })
    },
    replaceSidesContents: function () {
      console.log("sides function started");
      let menuItems = createArrFromText($sidesMenuParSelector, ', ');
      $sidesMenuContentSelector.empty();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($sidesMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text">' + value + '</span>');
      })
      console.log("sides function completed");
    },
    replaceSandwichesContents: function () {
      console.log("sandwich function running");
      let menuItems = createArrFromText($sandwichesMenuParSelector, ', ');
      $sandwichesMenuContentSelector.empty();
      $sandwichesMenuImageSelector.remove();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($sandwichesMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text">' + value + '</span>');
      })
      // make that bread
      $breadTopContainer = $('<div class="image-content">');
      $breadBottomContainer = $('<div class="image-content">');

      let $breadTop = $('<img />',
        {
          id: 'bread-top',
          src: themePath + '/img/bread-topp.png',
          class: 'bread-top'
        });

      let $breadBottom = $('<img />',
        {
          id: 'bread-bottom',
          src: themePath + '/img/bread-bot.png',
          class: 'bread-bottom'
        });
      $sandwichesMenuContentSelector.before($breadTopContainer.append($breadTop));
      $sandwichesMenuContentSelector.after($breadBottomContainer.append($breadBottom));
      console.log("sandwich function completed");
    },
    init: function () {
      $(window).load(
        restaurantFrontPageMenu.replaceDinnerContents,
        restaurantFrontPageMenu.replaceAppsContents,
        restaurantFrontPageMenu.replaceSidesContents,
        restaurantFrontPageMenu.replaceSandwichesContents
      );
    }
  };

  // restaurantFrontPageMenu.init();

  let appetizers = '.page-menu .menu-appetizers .js-menu-content p',
    dinner = '.page-menu .menu-dinner .js-menu-content p',
    sandwiches = '.page-menu .menu-sandwiches .js-menu-content p',
    sides = '.page-menu .menu-sides .js-menu-content p';

  let restaurantMenuPage = {
    createMenuList: function (menuSection) {
      let $menuSectionParent = $(menuSection).closest('.js-menu-content');
      console.log($menuSectionParent);
      let menuItems = createArrFromText(menuSection, ', ');
      $menuSectionParent.empty();
      console.log(menuItems);
      $ul = $('<ul class="menu-list">');
      $ul.appendTo($menuSectionParent);
      $.each(menuItems, function (index, value) {
        $ul.append('<li class="menu-item-text">' + value + '</li>');
      })
    },
    replaceMenuContents: function () {
      restaurantMenuPage.createMenuList(appetizers);
      restaurantMenuPage.createMenuList(dinner);
      restaurantMenuPage.createMenuList(sandwiches);
      restaurantMenuPage.createMenuList(sides);
    },
    init: function () {
      $(window).load(
        restaurantMenuPage.replaceMenuContents);
    }
  };


  // restaurantMenuPage.init();
});
