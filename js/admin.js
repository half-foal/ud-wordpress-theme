jQuery(document).ready(function ($) {

  // fixed header
  let $window = $(window),
    $body = $('body'),
    $header = $('header'),
    duration = 100;

  if ($body.hasClass('admin-bar-showing')) {
    var scrolling = false;

    $(window).scroll(function () {
      scrolling = true;
    });

    setInterval(function () {
      if (scrolling && $window.width() < 1200) {
        scrolling = false;
        $adminBarHeight = $('.admin-bar-showing').offset().top;
        if ($window.scrollTop() >= $adminBarHeight) {
          $('html').animate({
            marginTop: 0
          }, duration);
          $header.animate({
            top: 0
          }, duration);
        }
        if ($window.scrollTop() < $adminBarHeight) {
          $('html').animate({
            marginTop: $adminBarHeight
          }, duration);
          $header.animate({
            top: $adminBarHeight
          }, duration);
        }
      }
    }, 250);
  }
});