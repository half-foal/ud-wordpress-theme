jQuery(document).ready(function ($) {
  const themeName = 'UncleDanielsTheme';
  const themePath = './wp-content/themes/' + themeName;

  const createArrFromText = (target, splitter) => {
    let newArr = "";
    newArr = $(target).text().split(splitter);
    return newArr;
  };

  const chunkArr = (target, size) => {
    return target.reduce((memo, value, index) => {
      if (index % (target.length / size) == 0 && index !== 0) memo.push([])
      memo[memo.length - 1].push(value)
      return memo
    }, [[]])
  };
  const fadeInAfterLoad = (pageClassname) => {
    $(pageClassname).delay(300).animate({ opacity: 1 }, 1000, "linear");
  };
  let dinnerMenuSelector = '.menu-section-dinner',
    appsMenuSelector = '.menu-section-appetizers',
    sidesMenuSelector = '.menu-section-sides',
    sandwichesMenuSelector = '.menu-section-sandwiches';


  let $dinnerMenuContentSelector = $(dinnerMenuSelector).find('.js-menu-content'),
    $appsMenuContentSelector = $(appsMenuSelector).find('.js-menu-content'),
    $sidesMenuContentSelector = $(sidesMenuSelector).find('.js-menu-content'),
    $sandwichesMenuContentSelector = $(sandwichesMenuSelector).find('.js-menu-content');

  let $dinnerMenuParSelector = $dinnerMenuContentSelector.find('p'),
    $appsMenuParSelector = $appsMenuContentSelector.find('p'),
    $sidesMenuParSelector = $sidesMenuContentSelector.find('p'),
    $sandwichesMenuParSelector = $sandwichesMenuContentSelector.find('p');

  let $dinnerMenuImageSelector = $(dinnerMenuSelector).find('.image-content'),
    $appsMenuImageSelector = $(appsMenuSelector).find('.image-content'),
    $sidesMenuImageSelector = $(sidesMenuSelector).find('.image-content'),
    $sandwichesMenuImageSelector = $(sandwichesMenuSelector).find('.image-content');


  //front page menu 
  let restaurantFrontPageMenu = {
    fadeIn: function () {
      // let homePageContainerSelector = '.page-home';
      // $(homePageContainerSelector).delay(100).animate({ opacity: 1 }, 1000, "linear");
      fadeInAfterLoad('.page-home');
    },
    replaceDinnerContents: function () {
      let menuItems = createArrFromText($dinnerMenuParSelector, ', ');
      $dinnerMenuContentSelector.empty();
      $dinnerMenuImageSelector.remove();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($dinnerMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text">' + value + '</span>');
      })
      // make those faces
      $faceLeftContainer = $('<div class="image-content-left">');
      $faceRightContainer = $('<div class="image-content-right">');
      let $faceLeft = $('<img />',
        {
          id: 'face-left',
          src: themePath + '/img/clown-head.png',
          class: 'face-left'
        });

      let $faceRight = $('<img />',
        {
          id: 'face-right',
          src: themePath + '/img/clown-head.png',
          class: 'face-right'
        });
      $dinnerMenuContentSelector.before($faceLeftContainer.append($faceLeft));
      $dinnerMenuContentSelector.after($faceRightContainer.append($faceRight));
    },
    replaceAppsContents: function () {
      let menuItems = createArrFromText($appsMenuParSelector, ', ');
      $appsMenuContentSelector.empty();
      $appsMenuImageSelector.empty();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($appsMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text text-' + [index + 1] + '">' + value + '</span>');
      })
    },
    replaceSidesContents: function () {
      let menuItems = createArrFromText($sidesMenuParSelector, ', ');
      $sidesMenuContentSelector.empty();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($sidesMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text text-' + [index + 1] + '">' + value + '</span>');
      })
    },
    replaceSandwichesContents: function () {
      let menuItems = createArrFromText($sandwichesMenuParSelector, ', ');
      $sandwichesMenuContentSelector.empty();
      $sandwichesMenuImageSelector.remove();
      $div = $('<div class="menu-item-content">');
      $div.appendTo($sandwichesMenuContentSelector);
      $.each(menuItems, function (index, value) {
        $div.append('<span class="menu-item-text">' + value + '</span>');
      })
      // make that bread
      $breadTopContainer = $('<div class="image-content">');
      $breadBottomContainer = $('<div class="image-content">');

      let $breadTop = $('<img />',
        {
          id: 'bread-top',
          src: themePath + '/img/bread-topp.png',
          class: 'bread-top'
        });

      let $breadBottom = $('<img />',
        {
          id: 'bread-bottom',
          src: themePath + '/img/bread-bot.png',
          class: 'bread-bottom'
        });
      $sandwichesMenuContentSelector.before($breadTopContainer.append($breadTop));
      $sandwichesMenuContentSelector.after($breadBottomContainer.append($breadBottom));
    },
    init: function () {
      restaurantFrontPageMenu.replaceDinnerContents();
      restaurantFrontPageMenu.replaceAppsContents();
      restaurantFrontPageMenu.replaceSidesContents();
      restaurantFrontPageMenu.replaceSandwichesContents();
      restaurantFrontPageMenu.fadeIn();
    }
  };

  restaurantFrontPageMenu.init();
});