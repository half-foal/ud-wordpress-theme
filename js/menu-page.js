jQuery(document).ready(function ($) {

  const createArrFromText = (target, splitter) => {
    let newArr = "";
    newArr = $(target).text().split(splitter);
    return newArr;
  };

  const fadeInAfterLoad = (pageClassname) => {
    $(pageClassname).delay(300).animate({ opacity: 1 }, 1000, "linear");
  };

  let appetizers = '.page-menu .menu-appetizers .js-menu-content p',
    dinner = '.page-menu .menu-dinner .js-menu-content p',
    sandwiches = '.page-menu .menu-sandwiches .js-menu-content p',
    sides = '.page-menu .menu-sides .js-menu-content p';

  let restaurantMenuPage = {
    fadeIn: function () {
      fadeInAfterLoad('.page-menu');
    },
    createMenuList: function (menuSection) {
      let $menuSectionParent = $(menuSection).closest('.js-menu-content');
      let menuItems = createArrFromText(menuSection, ', ');
      $menuSectionParent.empty();
      $ul = $('<ul class="menu-list">');
      $ul.appendTo($menuSectionParent);
      $.each(menuItems, function (index, value) {
        $ul.append('<li class="menu-item-text">' + value + '</li>');
      })
    },
    replaceMenuContents: function () {
      restaurantMenuPage.createMenuList(appetizers);
      restaurantMenuPage.createMenuList(dinner);
      restaurantMenuPage.createMenuList(sandwiches);
      restaurantMenuPage.createMenuList(sides);
    },
    init: function () {
      restaurantMenuPage.replaceMenuContents();
      restaurantMenuPage.fadeIn();
    }
  };


  restaurantMenuPage.init();
});