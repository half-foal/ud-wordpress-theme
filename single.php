<? get_header(); ?>

<div class="wrapper">
  <div class="single-grid">
    <!-- left start -->
    <div class="blog-grid-left">
      <div class="blog-grid-item">
        <? 

          while(have_posts()) {
            the_post();
        ?>
        <div class="featured-image-container">
          <img class="featured-image" src="<? echo get_the_post_thumbnail_url(get_the_ID()); ?>"/>
        </div>
        <div class="post-content">
          <h2><? the_title(); ?></h2>
          <? the_content(); ?>
        </div>
        <?
          } // end while
          wp_reset_query();
        ?>
      </div>
    </div>
    <!-- left end -->
    <!-- right start -->
    <div class="blog-grid-right">
      <div class="blog-grid-right-title">
        <h3>Other News</h3>
      </div>

      <?
        $args = array(
          'post_type' => 'post',
          'posts_per_page' => 3,
          'post__not_in' => array( get_the_ID() ),
        );
        $otherposts = new WP_QUERY($args);
        if($otherposts->have_posts() ) {
          while ($otherposts->have_posts() ) {
            $otherposts->the_post();
      ?>
      <div class="blog-grid-item">
        <div class="post-info">
          <a href="<? the_permalink(); ?>">
            <h2 class="post-title">
              <? the_title(); ?>
            </h2>
          </a> 
          <div class="blog-excerpt">
            <!-- <p>
              <? echo wp_trim_words(get_the_excerpt(), 35); ?>
            </p> -->
            <div class="blog-category">
              <?
                if( !in_category( 'Uncategorized' ) ) {
                  $categories = get_the_category();
                }
                $separator = ' ';
                $output = '';
                if ( ! empty( $categories ) ) {
                  foreach( $categories as $category ) {
                    $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
                  }
                  echo trim( $output, $separator );
                }     
              ?>
            </div>
            <a class="text-link" href="<? the_permalink(); ?>">Read More</a>
          </div>
        </div>
      </div>
      <div class="grid-item-border-bottom">
        <div class="border-bottom"></div>
      </div>

    <?
        } //end while
      }  //end if
      wp_reset_query();
    ?>
    </div>
<!-- right end -->
  </div>
  </div>
 
</div>


<? get_footer(); ?>