
<?php
function theme_setup() {
  wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Bree+Serif|Open+Sans|Luckiest+Guy');
  wp_enqueue_style('style', get_stylesheet_uri(), NULL, microtime(), 'all');
  wp_enqueue_script("main", get_theme_file_uri('/js/main.js'), array('jquery'), microtime(), true);
  if( is_page('menu')) {
    wp_enqueue_script("menu-page", get_theme_file_uri('/js/menu-page.js'), array('jquery'), microtime(), true);
  }
  if( is_front_page() ){
    wp_enqueue_script("home-page", get_theme_file_uri('/js/home-page.js'), array('jquery'), microtime(), true);
  }
  if( current_user_can('editor') || current_user_can('administrator') ) { 
    wp_enqueue_script("admin", get_theme_file_uri('/js/admin.js'), array('jquery'), microtime(), true);
  }
}

add_action('wp_enqueue_scripts', 'theme_setup');


// Theme Support function

function theme_init() {
  add_theme_support('post-thumbnails');
  add_theme_support('title-tag');
  add_theme_support('html5', array('comment-list', 'comment-form', 'search-form'));
}

add_action('after_setup_theme', 'theme_init');


function theme_custom_post_type() {
  register_post_type('menus',
    array(
      'rewrite' => array('slug' => 'menu-section'),
      'labels' => array(
        'name' => 'Menus',
        'singular_name' => 'Menu Section',
        'add_new_item' => 'Add New Menu Section',
        'edit_item' => 'Edit Menu Section',
      ),
      'menu-icon' => 'dashicons-clipboard',
      'public' => true,
      'has_archive' => true,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
    )
  );
}

add_action('init', 'theme_custom_post_type');

