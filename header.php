<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php
    wp_head();
    if ( ! function_exists( '_wp_render_title_tag' ) ) {
      function theme_slug_render_title() {
    ?>
      <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php
	    }
	    add_action( 'wp_head', 'theme_slug_render_title' );
    }
    ?>
</head>
<body 
  class="<? if (is_admin_bar_showing()) { echo 'admin-bar-showing'; } ?>">

<header id="header-global">
  <div class="header-top">
    <div class="header-left">
      <a class="logo" href="<? echo site_url('') ?>">
        <h2>Uncle</h2>
        <h2>Daniel's</h2>
        <!-- <img src="<? echo get_template_directory_uri(); ?>/img/uncle-text.png" alt="Uncle Daniels"> -->
        <!-- <img src="<? echo get_template_directory_uri(); ?>/img/dudley-text.png" alt="Uncle Daniels"> -->
        <div class="logo-bg">
          <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/clown-flower.svg' ); ?>
          <!-- <img src="<? echo get_template_directory_uri(); ?>/img/logo-bg.png" alt="uncle daniels logo background"> -->
        </div>
      </a>
    </div>
    <div class="header-right">
      <a class="text-link" href="<? echo site_url('') ?>">Home</a>
      <a class="text-link" href="<? echo site_url('/menu') ?>">Our Menu</a>
      <a class="text-link" href="<? echo site_url('/catering') ?>">Catering</a>      
      <a class="text-link" href="<? echo site_url('/about') ?>">About</a>
      <a class="text-link" href="<? echo site_url('/blog') ?>">Blog</a>      
      <a class="text-link" href="<? echo site_url('/contact') ?>">Contact</a>
      <div class="menu-icon js-menu-icon text-white">
        <!-- <span class="menu-text">main menu</span> -->
          <img src="<? echo get_template_directory_uri(); ?>/img/fork-knife-plate.png" alt="main menu button">        
      </div>
    </div>
  </div>
  <div class="header-bottom">
    <div class="header-menu-mobile">
      <div class="nav-list">
        <ul>
          <li>
            <a class="text-link" href="<? echo site_url('') ?>">Home</a>
          </li>
          <li>
            <a class="text-link" href="<? echo site_url('/menu') ?>">Our Menu</a>
          </li>
          <li>
            <a class="text-link" href="<? echo site_url('/catering') ?>">Catering</a>      
          </li>
          <li>
            <a class="text-link" href="<? echo site_url('/about') ?>">About</a>
          </li>
          <li>
            <a class="text-link" href="<? echo site_url('/blog') ?>">Blog</a>      
          </li>
          <li>
            <a class="text-link" href="<? echo site_url('/contact') ?>">Contact</a>
          </li>
          <li>
            <div class="searchbox">
              <? get_search_form(); ?>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/mobile-menu-border.svg' ); ?>
</header>
<!-- getting a class to hook into on all my pages. -->
<? 
  global $wp;
  $current_slug = add_query_arg( array(), $wp->request );
  $home = "home";
  $single = "single";
  $search = "search";
?>
<main class="page-<? 
    if( is_search() ) {
      echo $search;
    }
    if( is_home() || is_front_page() ) { 
        echo $home ;
    // ignore custom page templates
    } else if (is_single() || is_singular() && !(is_page( array( 'menu', 'contact-us', 'blog', 'about' )))) {
        echo $single; }
    else {
      echo $current_slug;
    } 
  ?>">