<? get_header(); ?>

<!-- <div class="site-tagline-container">
  <svg viewBox="0 0 500 500">
    <path id="curve" d="M100,250 C100,72 395,74 400,250" />
      <text width="500">
        <textPath startOffset="50%" text-anchor="middle" xlink:href="#curve">
          <?
            echo get_bloginfo('description');
          ?>
        </textPath>
      </text>
    </svg>
</div> -->
<section class="wrapper site-description-container ta-center">
  <h1 class="text-huge site-description">
  <?
              echo get_bloginfo('description');
            ?>
  </h1>
</section>
<!-- menus start -->
<?php 
  $args = array(
    'post_type' => 'menus',
    'posts_per_page' => 5,
    'orderby' => 'menu_order', 
    'order' => 'ASC'
  );

  $menuposts = new WP_QUERY($args);
  if ( $menuposts->have_posts() ) {
    while ( $menuposts->have_posts() ) {
      $menuposts->the_post(); 
      ?>
      <section class="menu-section menu-section-<? echo strtolower(the_title('', '', false)); ?>">
        <div class="image-content">
          <img src="<? echo get_the_post_thumbnail_url(get_the_id()); ?>" />
        </div>
        <div class="title-badge-container">
          <div class="title-badge">
            <h2>
              <? the_title(); ?>
            </h2>
            <div class="badge-bg">
              <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/clown-flower.svg' ); ?>
            </div>
          </div>
        </div>
        <div class="menu-content js-menu-content">
            <? the_content(); ?>
        </div>
      </section>
      <div class="divider-section">
        <div class="divider-content">
          <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/section-border-zigzag.svg' ); ?>
          <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/section-border-zigzag.svg' ); ?>
        </div>
      </div>
    <?
    } // end while
  } // end if
  wp_reset_query();
?>
<!-- menus end -->


<? get_footer(); ?>

