<? get_header(); ?>

<?php 

$args = array(
  'post_type' => 'menus',
  'posts_per_page' => 3,
);

$menuposts = new WP_QUERY($args);
if ( $menuposts->have_posts() ) {
	while ( $menuposts->have_posts() ) {
		$menuposts->the_post(); 
    ?>
    <a href="<? the_permalink(); ?>">
    <h3>
      <? the_title(); ?>
    </h3>
    </a>
    <p>
      <? the_excerpt(); ?>
    </p>
  <?
	} // end while
} // end if
?>

<? get_footer(); ?>