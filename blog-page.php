<?php /* Template Name: Blog Page */ ?>
<? get_header(); ?>
<div class="page-title-container">
  <svg viewBox="0 0 500 500">
    <path id="curve" d="M100,250 C100,72 395,74 400,250" />
      <text width="500">
        <textPath startOffset="50%" text-anchor="middle" xlink:href="#curve">
          <? the_title(); ?>
        </textPath>
      </text>
    </svg>
</div>

<div class="grid-top-border">
  <div class="border-top"></div>
</div>
<div class="blog-section blog-grid wrapper">
  <!-- first post featured post section half the page on desktop -->
  <? 

    $args = array(
      'post_type' => 'post',
      'posts_per_page' => 1,
    );

    $blogposts = new WP_QUERY($args);
    if ( $blogposts->have_posts() ) {
      while ( $blogposts->have_posts() ) {
        $blogposts->the_post(); 
  ?>
  <div class="blog-grid-left">
    <div class="blog-grid-item">
      <div class="image-content">
        <a href="<? the_permalink(); ?>">
          <img src="<? echo get_the_post_thumbnail_url(get_the_id()); ?>" />
        </a>
      </div>
      <div class="blog-info">
        <a class="post-title-link" href="<? the_permalink(); ?>">
          <h2 class="post-title">
            <? the_title(); ?>
          </h2>
        </a> 
        <div class="blog-category">
          <?
            if( !in_category( 'Uncategorized' ) ) {
              $categories = get_the_category();
            }
            $separator = ' ';
            $output = '';
            if ( ! empty( $categories ) ) {
              foreach( $categories as $category ) {
                $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
              }
              echo trim( $output, $separator );
            }     
          ?>
        </div>
        <p class="blog-excerpt">
          <? echo wp_trim_words(get_the_excerpt(), 70); ?>
        </p>
        <a class="text-link read-more" href="<? the_permalink(); ?>">Read More</a>
      </div>
      <div class="grid-item-border-bottom">
        <div class="border-bottom"></div>
      </div>
    </div>
  </div>
  <?
      } // end while
    } // end if
    wp_reset_query();
  ?>

  <!-- Secondary featured post with thumbnail image to be shown above rest of posts and to the right of the main post -->
  <div class="blog-grid-right">
    <!-- Rest of the posts section where each post is just a title and excerpt and category -->
    <? 

      $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'offset' => 1
      );

      $blogposts = new WP_QUERY($args);
      if ( $blogposts->have_posts() ) {
        while ( $blogposts->have_posts() ) {
          $blogposts->the_post(); 
    ?>  
      <div class="blog-grid-item">
        <div class="image-content">
          <a href="<? the_permalink(); ?>">
            <img src="<? echo get_the_post_thumbnail_url(get_the_id()); ?>" />
          </a>
        </div>
        <div class="blog-info">
          <a class="post-title-link" href="<? the_permalink(); ?>">
            <h2 class="post-title">
              <? the_title(); ?>
            </h2>
          </a> 
          <div class="blog-category">
            <?
              if( !in_category( 'Uncategorized' ) ) {
                $categories = get_the_category();
              }
              $separator = ' ';
              $output = '';
              if ( ! empty( $categories ) ) {
                foreach( $categories as $category ) {
                  $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
                }
                echo trim( $output, $separator );
              }     
            ?>
          </div>
          <p class="blog-excerpt">
            <? echo wp_trim_words(get_the_excerpt(), 18); ?>
          </p>
          <a class="text-link read-more" href="<? the_permalink(); ?>">Read More</a>
        </div>
      </div>
      <!-- end of blog item -->
      <div class="grid-item-border-bottom">
        <div class="border-bottom"></div>
      </div>
      
      <?
    } // end while
  } // end if
  ?>
  </div>
</div>
<section class="wrapper">
  <div class="blog-section blog-grid-bottom">
    <? 
      $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'offset' => 4
      );

    $blogposts = new WP_QUERY($args);
    if ( $blogposts->have_posts() ) {
      while ( $blogposts->have_posts() ) {
        $blogposts->the_post(); 
    ?>  
  <div class="blog-grid-item">
    <div class="image-content">
      <a href="<? the_permalink(); ?>">
        <img src="<? echo get_the_post_thumbnail_url(get_the_id()); ?>" />
      </a>
    </div>
    <div class="blog-info">
      <a class="post-title-link" href="<? the_permalink(); ?>">
        <h2 class="post-title">
          <? the_title(); ?>
        </h2>
      </a> 
      <div class="blog-category">
        <?
          if( !in_category( 'Uncategorized' ) ) {
            $categories = get_the_category();
          }
          $separator = ' ';
          $output = '';
          if ( ! empty( $categories ) ) {
            foreach( $categories as $category ) {
              $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
            }
            echo trim( $output, $separator );
          }     
        ?>
      </div>
      <p class="blog-excerpt">
        <? echo wp_trim_words(get_the_excerpt(), 25); ?>
      </p>
      <a class="text-link read-more" href="<? the_permalink(); ?>">Read More</a>
    </div>
    <!-- end of blog item -->
  </div>
    <?
        } // end while
      } // end if
    ?>
  </div>
  <div class="grid-item-border-bottom">
      <div class="border-bottom"></div>
    </div>
</section>

<section class="wrapper rest-of-blog-posts">

<div>
    <? 

      $args = array(
        'post_type' => 'post',
        'offset' => 7
      );

      $blogposts = new WP_QUERY($args);
      if ( $blogposts->have_posts() ) {
        while ( $blogposts->have_posts() ) {
          $blogposts->the_post(); 
    ?>  
      <div class="blog-grid-item">
        <div class="image-content">
          <a href="<? the_permalink(); ?>">
            <img src="<? echo get_the_post_thumbnail_url(get_the_id()); ?>" />
          </a>
        </div>
        <div class="blog-info">
          <a class="post-title-link" href="<? the_permalink(); ?>">
            <h2 class="post-title">
              <? the_title(); ?>
            </h2>
          </a> 
          <div class="blog-category">
            <?
              if( !in_category( 'Uncategorized' ) ) {
                $categories = get_the_category();
              }
              $separator = ' ';
              $output = '';
              if ( ! empty( $categories ) ) {
                foreach( $categories as $category ) {
                  $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
                }
                echo trim( $output, $separator );
              }     
            ?>
          </div>
          <p class="blog-excerpt">
            <? echo wp_trim_words(get_the_excerpt(), 18); ?>
          </p>
          <a class="text-link read-more" href="<? the_permalink(); ?>">Read More</a>
        </div>
      </div>
      <!-- end of blog item -->
      <div class="grid-item-border-bottom">
        <div class="border-bottom"></div>
      </div>
      
      <?
    } // end while
  } // end if
  ?>
  </div>

</section>

<? get_footer(); ?>