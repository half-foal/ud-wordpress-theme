<? get_header(); ?>
  <section class="wrapper search-results-container mt-header">
    <div class="search-results-inner">
      <h2>Results for "<? echo get_search_query(); ?>"</h2>
      <ul>
      <?
        if (have_posts()) {

          while ( have_posts() ) {
            the_post(); 
      ?>

      <li class="search-result-line-item">
        <a class="text-link" href="<? the_permalink(); ?>">
          <? the_title(); ?>
        </a>
      </li>
        <?
          } // end while 
        ?>
          </ul>
      <? 
        } else {
      ?>
        <div class="searchbox">
          <? get_search_form(); ?>
        </div>
        <p>Oops! Looks like there are no results for "<? echo get_search_query(); ?>"</p>
      <?
        } // end else
      ?>
    </div>

  </section>

<? get_footer(); ?>