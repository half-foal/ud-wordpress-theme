<? 
  // throwing in our catering link on all pages except... catering
  if (!is_page('catering')) {
?>
    <!-- catering link start-->
    <!-- we want this here bc we actually want it everywhere... yeah -->
    <section class="section-catering">
      <div class="catering-badge-container">
        <div class="catering-badge">
          <div class="badge-title">
          <a class="text-link text-link-white" href="<? echo site_url('/catering') ?>">
            <h2 class="text-white">We Do Catering!</h2>
          </a>      


          </div>
          <div class="badge-bg">
            <img src="<? echo get_template_directory_uri(); ?>/img/catering-bg.png" alt="Uncle Daniels" class="catering-bg">
          </div>
        </div>
      </div>
    </section>
    <!-- catering link end -->
<? 
  } // end if
?>
</main>
<footer id="footer-global" class="text-white">
    <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/border-top.svg' ); ?>
    <div class="footer-inner">
      <div class="footer-left">
          <!-- <?php echo file_get_contents( get_stylesheet_directory_uri() . '/img/svg/clown-whole-face.svg' ); ?> -->
            <img src="<? echo get_template_directory_uri(); ?>/img/goat-head.png" alt="Uncle Daniels" class="goat-head">
      </div>
      <div class="footer-middle-left">
        <div class="footer-menu-list">
          <h2>Site Map</h2>
          <ul>
            <li>
              <a class="text-link text-link-white" href="<? echo site_url('') ?>">Home</a>
            </li>
            <li>
              <a class="text-link text-link-white" href="<? echo site_url('/blog') ?>">Blog</a>      
            </li>
            <li>
              <a class="text-link text-link-white" href="<? echo site_url('/menu') ?>">Our Menu</a>
            </li>
            <li>
              <a class="text-link text-link-white" href="<? echo site_url('/about') ?>">About</a>
            </li>
            <li>
              <a class="text-link text-link-white" href="<? echo site_url('/contact') ?>">Contact</a>
            </li>
            <li>
              <div class="searchbox">
                <? get_search_form(); ?>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="footer-middle-right">
        <div class="footer-menu-list">
          <ul>
            <h2>Our Blog</h2>
            <!-- blog posts -->
            <?
              $args = array(
                'post_type' => 'post',
                'posts_per_page' => 6
              );

            $blogposts = new WP_QUERY($args);
            if ( $blogposts->have_posts() ) {
              while ( $blogposts->have_posts() ) {
                $blogposts->the_post(); 
            ?>
            <li>
              <a class="text-link text-link-white" href="<? the_permalink(); ?>">
                <? the_title(); ?>
              </a> 
            </li>
            <?
                } // end while
              } // end if
              wp_reset_query();
            ?>
          </ul>
        </div>
      </div>
      <div class="footer-right">
        <div class="footer-menu-list">
          <h2>Visit Us</h2>
          <ul>
            <li>
            1785  Bates Brothers Road 
            <br>
            Rocky River, OH 442423
            </li>
            <li>
            614-268-0663
            </li>
            <li>
          uncledanielscatering.com
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- Scripts -->
  <? wp_footer(); ?>
  </body>
</html>