<?php /* Template Name: Menu Page */ ?>

<? get_header(); ?>

<div class="page-title-container">
  <svg viewBox="0 0 500 500">
    <path id="curve" d="M100,250 C100,72 395,74 400,250" />
      <text width="500">
        <textPath startOffset="50%" text-anchor="middle" xlink:href="#curve">
          Our Menu
        </textPath>
      </text>
    </svg>
</div>
<div class="grid two-columns">
  <? 
    $args = array(
      'post_type' => 'menus',
      'posts_per_page' => 6,
      'orderby' => 'menu_order', 
      'order' => 'ASC'
    );

    $menuposts = new WP_QUERY($args);
    if ( $menuposts->have_posts() ) {
      while ( $menuposts->have_posts() ) {
        $menuposts->the_post(); 
        ?>
          <div class="menu-section ta-center menu-<? echo strtolower(get_the_title()); ?>">
            <h2 class="menu-title heading-text-underline"><? the_title(); ?></h2>
            <div class="menu-content js-menu-content">
                <? the_content(); ?>
            </div>
          </div>
          
          <?
      } // end while
    } // end if
    wp_reset_query();
    ?>
  </div>


<? get_footer(); ?>
